import java.lang.*;
import edu.princeton.cs.algs4.*;
import java.util.*;

public class FastCollinearPoints {
  private static class Byy implements Comparator<Point> {
    @Override
    public int compare(Point a,Point b) {
      return a.compareTo(b);
    }
  }
  private static final Comparator<Point> BYY = new Byy();
  private ResizingArrayBag<LineSegment> bg;
  private int sizeofs;

  public FastCollinearPoints(Point[] points) {     // finds all line segments containing 4 or more points

    if(points == null )
      throw new IllegalArgumentException();
    int ps = points.length;
    for(int test = 0; test < points.length;test++) {
      if(points[test] == null) 
        throw new IllegalArgumentException();
    }

    for(int test = 0; test < points.length-1;test++) {
      for(int test2 = test + 1; test2 < points.length; test2++) {
        if(Double.compare(points[test].slopeTo(points[test2]),Double.NEGATIVE_INFINITY)==0)
          throw new IllegalArgumentException();
      }
    } 


    Point[] tobp = new Point[points.length];

    System.arraycopy(points,0,tobp,0,points.length);


    bg = new ResizingArrayBag<LineSegment>();
    Arrays.sort(tobp, BYY);

    Point[] sam = new Point[ps];


//        for(int aa = 0;aa < ps;aa++) {
//          StdOut.println(tobp[aa]);
//          }
//        StdOut.println("*****");

    
    for(int i = 0; i  < ps - 3; i++) {


      System.arraycopy(tobp,0,sam,0,ps);

      if(i>1)
        Arrays.sort(sam,0,i,tobp[i].slopeOrder());  // before i  
      Arrays.sort(sam, i+1, ps, tobp[i].slopeOrder());  //after i
//            StdOut.println("i element");
//            for(int aa = 0;aa < ps;aa++) {
//              StdOut.println(sam[aa]);
//              StdOut.println(sam[i].slopeTo(sam[aa]));
//            }
      int accu;
      int tr = 0;
      for(int z = i+1; z < ps - 2 ; z += accu )  {
        accu = 1;
        int ind = z;
        double ftan = tobp[i].slopeTo(sam[ind]);
             //    StdOut.println("init");
        //      StdOut.println(sam[ind]);
        while(++ind < ps  && 0 == Double.compare(tobp[i].slopeTo(sam[ind]),ftan)) {
          accu++;
            // StdOut.println(sam[ind]);
        } 
            // StdOut.println("---------------");
        if(accu >= 3){ 
           //  StdOut.println("------YYYYYY------");
        Point end = sam[ind -1];

          boolean dup = false;
          for(; tr < i; tr++) {
            int v = Double.compare(tobp[i].slopeTo(sam[tr]),ftan);
            if(v <  0)
              continue;
            if(v == 0) {
              dup = true;
              tr++;
              break;
            }
            if(v > 0) 
              break;
          }
          if(dup) continue;

          bg.add(new LineSegment(sam[i],end));
        }
      }

    }
    sizeofs = bg.size();
  }





  public  int numberOfSegments() {        // the number of line segments
    return sizeofs;
  }


  public LineSegment[] segments() {                // the line segments
    LineSegment[] r = new LineSegment[sizeofs];
    int ssz = 0;
    for(LineSegment one : bg) {
      r[ssz++] = one;
    }
    return r;
  }

  public static void main(String[] args) {

    //  read the n points from a file
    In in = new In(args[0]);
    int n = in.readInt();
    Point[] points = new Point[n];
    for (int i = 0; i < n; i++) {
      int x = in.readInt();
      int y = in.readInt();
 
      points[i] = new Point(x, y);
    }

    //                                                  draw the points
    StdDraw.enableDoubleBuffering();
    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);
    for (Point p : points) {
      p.draw();
    }
    StdDraw.show();

    //                                                                                    print and draw the line segments
    FastCollinearPoints collinear = new FastCollinearPoints(points);
    for (LineSegment segment : collinear.segments()) {
      StdOut.println(segment);
      segment.draw();
    }
    StdDraw.show();
  }
}
