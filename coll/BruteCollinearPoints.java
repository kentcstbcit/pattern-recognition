import java.lang.*;
import java.util.*;
import edu.princeton.cs.algs4.*;

public class BruteCollinearPoints {
  private static class Byy implements Comparator<Point> {
    @Override
    public int compare(Point a,Point b) {
      return a.compareTo(b);
    }
  }

  private static final Comparator<Point> BYY = new Byy();
 
  private ResizingArrayBag<LineSegment> lss;
  private  int numofSes;
  public BruteCollinearPoints(Point[] points) {    //  finds all line segments containing 4 points
    lss = new ResizingArrayBag<LineSegment>();
    if(points ==  null)
      throw new IllegalArgumentException();
    int n_t = 0;

    for(int test = 0; test < points.length;test++) {
      if(points[test] == null) 
        throw new IllegalArgumentException();
    }

    for(int test = 0; test < points.length-1;test++) {
      for(int test2 = test + 1; test2 < points.length; test2++) {
        if(Double.compare(points[test].slopeTo(points[test2]),Double.NEGATIVE_INFINITY)==0)
          throw new IllegalArgumentException();
      }
    } 

    Point[] tobp = new Point[points.length];

    System.arraycopy(points,0,tobp,0,points.length);

    Arrays.sort(tobp, BYY);
    for(int i = 0; i < tobp.length -3 ; i++) {

      for(int j = i+1; j < tobp.length -2; j++) {

        boolean du = false;
        for(int tr =0; tr < i;tr++) {
          if(0==Double.compare(tobp[tr].slopeTo(tobp[j]),tobp[i].slopeTo(tobp[j])))
            du = true;
          break;
        }
        if(du)
          continue;


        for(int k = j+1; k < tobp.length -1 ; k++) {

          if(0!=Double.compare(tobp[i].slopeTo(tobp[j]),tobp[i].slopeTo(tobp[k])))
            continue;

          boolean duplicate = false;
          for(int tr = 0; tr < j; tr++) {
            if(tr != i && 
                0==Double.compare(tobp[tr].slopeTo(tobp[k]),tobp[i].slopeTo(tobp[j]))){
              duplicate = true;
              break;
                }
          }

          if(duplicate)
            continue;

          int z;
          for(z = tobp.length -1; z > k ; z--) {
            if(0==Double.compare(tobp[i].slopeTo(tobp[j]),tobp[i].slopeTo(tobp[z]))) {
              break;
            } 
          }
          if(z > k) { 
            boolean dupl = false;

            for(int tr = 0; tr < k;tr++) {
              if(tr != i 
                  && tr !=j 
                  && 0==Double.compare(tobp[tr].slopeTo(tobp[z]),tobp[i].slopeTo(tobp[z]))){
                dupl = true;
                  }
            }
            if(!dupl)

              lss.add(new LineSegment(tobp[i], tobp[z]));
          }
        }

      }
    }

    numofSes = lss.size();
  }

  public int numberOfSegments() {         // the number of line segments
    return numofSes;

  }

  public LineSegment[] segments() {          //        the line segments
    int c = 0;
    LineSegment[] n = new LineSegment[lss.size()];
    for(LineSegment o : lss) {
      n[c++] = o;
    }
    return n;
  }
  public static void main(String[] args) {

    //  read the n points from a file
    In in = new In(args[0]);
    int n = in.readInt();
    Point[] points = new Point[n];
    for (int i = 0; i < n; i++) {
      Integer x = in.readInt();
      Integer y = in.readInt();
      if(x ==  null || y == null)
        throw new IllegalArgumentException();
    
      points[i] = new Point(x, y);
    }

    //                                                  draw the points
    StdDraw.enableDoubleBuffering();
    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);
    for (Point p : points) {
      p.draw();
    }
    StdDraw.show();

    //                                                                                    print and draw the line segments
    BruteCollinearPoints collinear = new BruteCollinearPoints(points);
    for (LineSegment segment : collinear.segments()) {
      StdOut.println(segment);
      segment.draw();
    }
    StdDraw.show();
  }
}
